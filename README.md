# ICMI2024 Knudsen et al Poster presentation

Hello there! Thanks for stopping by and checking out our latest results :) For more information you can contact me at [christelle.knudsen\@inrae.fr](mailto:christelle.knudsen@inrae.fr){.email}

## Abstract

### Single cell transcriptomics reveal that initiation of solid food intake triggers the maturation of caecal *lamina propria* immune cells in suckling rabbits

The weaning transition from milk to solid food intake is key to the co-maturation of the gut immune system and microbiota. However, little is known on the direct effect of the initiation of solid food intake on the maturation of the immune cells. Here, we propose to evaluate its impact on the transcriptome of digestive immune cells using single cell RNA-sequencing (scRNAseq).

Rabbit pups were either exclusively suckled or suckled and fed solid food from 12 days of age. At 25 days of age, mononuclear cells (n = 4/group) were isolated from the caecal *lamina propria*. Single live CD45^+^ cells were sorted by flow cytometry and droplet-based scRNAseq was performed. Data were analyzed using the Cell Ranger software and the R package Seurat. After applying quality filters, an average of 4,628 cells per sample were kept. 20 cell clusters were identified, with the majority affiliated to macrophages, dendritic cells, T and B cells based on the expression of canonical markers (*CD14*, *CD68*, *CST3*, *CD3D*, *JCHAIN*). Solid food introduction affected both cell proportions and transcriptomes. Namely, a marked switch in B cell populations was observed, with a strong reduction in the proportion of naive B cells (11 vs 3%) and an increase in that of mature plasma cells (2 vs 6%).\
Our results highlight the crucial role of the initiation of solid food intake for the maturation of the digestive mucosal immune system, and pave the way for mechanistic studies unravelling the role of the gut microbiota in this developmental process.

## Authors and acknowledgment

KNUDSEN Christelle^1^, MALONGA Tania^1^, RUMEAU Mathilde^1^, LHUILLIER Emeline^2,3^, CABAU Cédric^1^, ZAKAROFF-GIRARD Alexia^2,4^, RIANT Elodie^2,4^, AYMARD Patrick^1^, COMBES Sylvie^1^, BEAUMONT Martin^1^

1 - GenPhySE, Université de Toulouse, INRAE, ENVT, F-31326, Castanet-Tolosan, France.

2 - Institut des Maladies Métaboliques et Cardiovasculaires (I2MC), INSERM UMR 1297, 31432 Toulouse, France.

3 - Plateforme GeT, Genotoul, 31100 Toulouse, France.

4 - Plateforme Cytometry TRI-FBI, Genotoul, 31100 Toulouse, France.
